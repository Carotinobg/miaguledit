-- Editor
io.stdout:setvbuf("no")

lg = love.graphics
lw = love.window

SCREEN_WIDTH = 1280
SCREEN_HEIGHT = 720

-- Carico la palette
require "palette"

-- Costruisco la palette
palette = Palette()

-- Canvas
require "canvas"
canvas = Canvas(64, 64, palette)

-- Salvataggio immagine
function salva()
  local nome_file = "immagine"
  local file = io.open(nome_file, "w")
  for i = 1, canvas.W do
    local primo = true
    for j = 1, canvas.H do
      if not primo then
        file:write(" ")
      end
      primo = false
      file:write(canvas.GRID[i][j])      
    end
    file:write("\n")
  end
  file:flush()
  io.close(file)
  print("File salvato:", nome_file)
end

function carica()
  local nome_file = "immagine"
  
  local file = io.open(nome_file, "r")
  
  local riga = file:read()
  local contatore_riga = 1
  local contatore_colonna = 1
  while riga ~= nil do
    contatore_colonna = 1
    local stringa = ""
    for i in string.gmatch(riga, "%S+") do
      canvas.GRID[contatore_riga][contatore_colonna] = tonumber(i)
      stringa = stringa .. i .. " "
      contatore_colonna = contatore_colonna + 1
    end
    print(stringa)
    contatore_riga = contatore_riga + 1
    riga = file:read()
  end
  
  io.close(file)
  
  print("File caricato: ", nome_file)
end

function love.load()
  lw.setMode(SCREEN_WIDTH, SCREEN_HEIGHT, {vsync = true})
  love.keyboard.setKeyRepeat(true)
end

function love.update(dt)
  palette:update(dt) 
  canvas:update(dt)
end

function love.draw()
  palette:draw(10, 10)
  canvas:draw(500, 10)
end

function love.keyreleased(key, scancode)
    if key == "p" then      
      palette:next_color()      
    end
    
    if key == "o" then
      palette:prev_color()
    end
    
    if key == "b" then
      canvas:fill()
    end
    
    if scancode == "f1" then
      salva()
    end
    
    if scancode == "f4" then
      carica()
    end
    
    if key == "l" then
      canvas:toggle_line_mode()
    end
    
    if key == "u" then
      canvas:undo()
    end
end

function love.keypressed(key, scancode, isrepeat)
  if key == "left" then
    canvas:move_cursor(key)
  end
  
  if key == "right" then
    canvas:move_cursor(key)
  end
  
  if key == "up" then
    canvas:move_cursor(key)
  end
  
  if key == "down" then
    canvas:move_cursor(key)
  end
  
  if key == "d" then
    canvas:toggle_paint_mode()
  end
  
  if key == "s" then
    canvas:normal_mode()
  end
  
  if key == "a" then
    canvas:sample()
  end
end