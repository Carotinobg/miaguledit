-- Palette
Palette = {}
Palette.__index = Palette

setmetatable(Palette, {
  __call = function(cls, ...)
    return cls.new(...)
    end,
})

function Palette.new()
  local self = setmetatable({}, Palette)
  
  self.PALETTE = {}
  function crea_colore(r, g, b)    
    table.insert(self.PALETTE, {r = r, g = g, b = b})
  end
  crea_colore(0, 0, 0)
  crea_colore(255, 255, 255)
  crea_colore(136, 0, 0)
  crea_colore(170, 255, 238)
  crea_colore(204, 68, 204)
  crea_colore(0, 204, 85)
  crea_colore(0, 0, 170)
  crea_colore(238, 238, 119)
  crea_colore(221, 136, 85)
  crea_colore(102, 68, 0)
  crea_colore(255, 119, 119)
  crea_colore(51, 51, 51)
  crea_colore(119, 119, 119)
  crea_colore(170, 255, 102)
  crea_colore(0, 136, 255)
  crea_colore(187, 187, 187)
  
  self.COLOR = 1

  for _, p in ipairs(self.PALETTE) do
    print(p.r, p.g, p.b)
  end
  print("Fine costruttore di Palette")
  
  return self
end

function Palette:get_active_color_index()
  return self.COLOR
end

function Palette:get_active_color()  
  return self.PALETTE[self.COLOR]
end

function Palette:set_active_color(c)
  if c < 1 then
    c =1
  end
  
  if c > 16 then
    c = 16
  end
  
  self.COLOR = c
end

function Palette:get_color(index)
  if index < 1 then
    index = 1
  end
  
  if index > 16 then
    index = 16
  end
  
  return self.PALETTE[index]
end

function Palette:next_color()
  self.COLOR = (self.COLOR % 16) + 1
end

function Palette:prev_color()
    self.COLOR = self.COLOR - 1
    if self.COLOR == 0 then
      self.COLOR = 16
    end
end

function Palette:update(dt)
  
end

function Palette:draw(x, y)
  -- Salvo i colori iniziali
  local ir,ig,ib,ia = lg.getColor()
  
  
  local cell_width = 64
  local border_width = 4
  
  -- Disegno un rettangolo contenitore
  lg.setColor(255, 255, 255)
  lg.rectangle("fill", x, y, border_width * 2 + cell_width * 2, border_width * 2 + cell_width * 8)
  
  local left_col = x + border_width
  local right_col = left_col + cell_width
  for i = 1, 8 do
    local left_color = self.PALETTE[0 + i]
    local right_color = self.PALETTE[8 + i]
    
    -- Quadrato di sinistra
    lg.setColor(left_color.r, left_color.g, left_color.b)
    lg.rectangle("fill", left_col, y + border_width + (i - 1) * cell_width, cell_width, cell_width)
    
    lg.setColor(right_color.r, right_color.g, right_color.b)
    lg.rectangle("fill", right_col, y + border_width + (i - 1) * cell_width, cell_width, cell_width)
  end
  
  -- Evidenzio il colore attivo
  local colonna = x + math.floor((self.COLOR - 1)/ 8) * cell_width + border_width + cell_width / 2
  local riga = y + ((self.COLOR - 1) % 8) * cell_width + border_width + cell_width / 2
  local colore = self:get_active_color()  
  lg.setColor(255 - colore.r, 255 - colore.g, 255 - colore.b)
  lg.circle("fill", colonna, riga, cell_width / 4)

  
  -- Ripristino il colore iniziale
  lg.setColor(ir, ig, ib, ia)
end