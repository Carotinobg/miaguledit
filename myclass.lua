MyClass = {}
MyClass.__index = MyClass

setmetatable(MyClass, {
  __call = function (cls, ...)
    return cls.new(...)
  end,
})

function MyClass.new(init)
  local self = setmetatable({}, MyClass)
  self.value = init
  
  self.COLOR = 1
  
  return self
end

-- the : syntax here causes a "self" arg to be implicitly added before any other args
function MyClass:set_value(newval)
  self.value = newval
end

function MyClass:get_value()
  return self.value
end

function MyClass:next_color()
  self.COLOR = (self.COLOR % 16) + 1
end

function MyClass:prev_color()
  self.COLOR = self.COLOR - 1
    if self.COLOR == 0 then
      self.COLOR = 16
    end
end

function MyClass:get_color()
  return self.COLOR
end
