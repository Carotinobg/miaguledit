Canvas = {}
Canvas.__index = Canvas

NORMAL, PAINT, LINE = "normal", "paint", "line"

setmetatable(Canvas, {
  __call = function (cls, ...)
    return cls.new(...)
  end,
})

function Canvas.new(w, h, palette)
  local self = setmetatable({}, Canvas)
  self.W = w
  self.H = h
  self.palette = palette
  
  -- inizializzo la griglia con il primo colore
  self.GRID = {}
  for i = 1, self.W do
    self.GRID[i] = {}
    for j = 1, self.H do
--      table.insert(self.GRID[i], math.random(16))
      table.insert(self.GRID[i], 1)
    end
  end
  
  self.CELL_WIDTH = 8
  
  -- Posizione di edit corrente
  self.pos = {x = 1, y = 1}
  
  -- Modalità
  self.state = NORMAL  
  
  -- Timer per il cursore
  self.cursore = {}
  self.cursore.timer = 0
  self.cursore.time = 4 / 16
  self.cursore.colore = 1
  
  -- Utilità per disegnare linee
  -- Quando diventa true, la linea inizia dal punto in cui mi trovo
  -- Poi muovo il cursore e quando premo di nuovo il tasto finalizzo  
  self.linea = {}
  self.linea.inizio = {}
  self.linea.fine = {}
  
  -- Sprite Batch per velocizzare
  self.sprite_batch = lg.newSpriteBatch(lg.newImage("1pxwhite.png"), 64 * 64, "dynamic")
  
  -- Struttura per UNDO
  self.undo_fw = {}
  self.undo_fw.transaction = false
  self.undo_fw.old = {}
  self.undo_fw.todo = {}
  
  return self
end

function Canvas:start_transaction()
  if self.undo_fw.transaction == false then    
    self.undo_fw.transaction = true
    
    -- Pulisco la tabella delle modifiche da eseguire
    for k,v in pairs(self.undo_fw.todo) do
      self.undo_fw.todo[k] = nil
    end
  end
end

function Canvas:commit()
  if self.undo_fw.transaction  == true then    
    self.undo_fw.transaction = false
    
    -- Pulisco la tabella delle modifiche eseguite
    for k,v in pairs(self.undo_fw.old) do
      self.undo_fw.old[k] = nil
    end
    
    while #self.undo_fw.todo > 0 do
      local m = table.remove(self.undo_fw.todo)
      local x = m.x
      local y = m.y
      local i = m.i
      table.insert(self.undo_fw.old, {x = x, y = y, i = self.GRID[x][y]})
      self.GRID[x][y] = i
    end
  end
end

function Canvas:undo()
  
  -- Pulisco la tabella dei vecchi
  print("Undo", #self.undo_fw.old)
  
  while #self.undo_fw.old > 0 do
    local u = table.remove(self.undo_fw.old)
    local x = u.x
    local y = u.y
    local i = u.i
    
    self.GRID[x][y] = i
  end
  
end

function Canvas:put_pixel_now(x, y, c)
  self:start_transaction()
  
  self:put_pixel(x, y, c)
  
  self:commit()
end

function Canvas:put_pixel(x, y, c)
  table.insert(self.undo_fw.todo, {x = x, y = y, i = c})
end

-- the : syntax here causes a "self" arg to be implicitly added before any other args
function Canvas:update(dt)
  self.cursore.timer = self.cursore.timer + dt
  if self.cursore.timer > self.cursore.time then
    self.cursore.timer = self.cursore.timer - self.cursore.time
    self.cursore.colore = (self.cursore.colore % 16) + 1
  end
end

function Canvas:toggle_paint_mode()
  if self.state == PAINT then    
    self.state = NORMAL
  else
    self.state = PAINT
  end
  
  if self.state == PAINT then
    self:put_pixel_now(self.pos.x, self.pos.y, self.palette:get_active_color_index())
  end
end

function Canvas:toggle_line_mode()
  if self.state ~= LINE then
    print("Entro in modo linea")
    self.state = LINE
  
    -- Salvo le coordinate 
    self.linea.inizio.x = self.pos.x
    self.linea.inizio.y = self.pos.y
  else
    print("Disegno la linea")
    self.state = NORMAL
    
    -- Attivo la transazione
    self:start_transaction()
    
    self.linea.fine.x = self.pos.x
    self.linea.fine.y = self.pos.y
    -- Disegno la riga
    -- Prima valuto i casi particolari
    -- Riga verticale
    local start = self.linea.inizio
    local fine = self.linea.fine
    local colore = self.palette:get_active_color_index()
    -- Riga verticale
    if start.x == fine.x then
      local a, b
      if start.y > fine.y then
        a = fine.y
        b = start.y
      else
        a = start.y
        b = fine.y
      end
      for i = a, b do
--        self.GRID[start.x][i] = colore
        self:put_pixel(start.x, i, colore)
      end      
    elseif start.y == fine.y then
      -- Riga orizzontale
      local a, b
      if start.x > fine.x then
        a = fine.x
        b = start.x
      else
        a = start.x
        b = fine.x
      end
      for i = a, b do
--        self.GRID[i][start.y] = colore
        self:put_pixel(i, start.y, colore)
      end
    else
      
      if math.abs(fine.y - start.y) < math.abs(fine.x - start.x) then
        if start.x > fine.x then
          self:line_low(fine.x, fine.y, start.x, start.y, colore)
        else
          self:line_low(start.x, start.y, fine.x, fine.y, colore)
        end
      else
        if start.y > fine.y then
          self:line_high(fine.x, fine.y, start.x, start.y, colore)
        else
          self:line_high(start.x, start.y, fine.x, fine.y, colore)
        end
        
      end
    end
    
    self:commit()
    
  end
end

function Canvas:line_low(x0, y0, x1, y1, colore)
  print("line_low")
  local dx = x1 - x0
  local dy = y1 - y0
  local yi = 1
  if dy < 0 then
    yi = -1
    dy = -dy
  end
  
  local D = 2 * dy - dx
  local y = y0
  
  for x = x0, x1 do
--    self.GRID[x][y] = colore
    self:put_pixel(x, y, colore)
    if D > 0 then
      y = y + yi
      D = D - 2 * dx
    end
    D = D + 2 * dy
  end
end

function Canvas:line_high(x0, y0, x1, y1, colore)
  print("line_high")
  local dx = x1- x0
  local dy = y1 - y0
  local xi = 1
  if dx < 0 then
    xi = -1
    dx = -dx
  end
  
  local D = 2 * dx - dy
  local x = x0
  
  for y = y0, y1 do
--    self.GRID[x][y] = colore
    self:put_pixel(x, y, colore)
    if D > 0 then
      x = x + xi
      D = D - 2 * dy
    end
    D = D + 2 * dx
  end
end


function Canvas:move_cursor(dir)  
  if dir == "left" then
    self:move_left()
  elseif dir == "right" then
    self:move_right()
  elseif dir == "up" then
    self:move_up()
  elseif dir == "down" then
    self:move_down()
  end
  
  if self.state == PAINT then
    self:put_pixel_now(self.pos.x, self.pos.y, self.palette:get_active_color_index())
  end
  
--  print(dir, self.pos.x, self.pos.y)
end

function Canvas:move_left()
  self.pos.x = self.pos.x - 1
  if self.pos.x < 1 then
    self.pos.x = self.W
  end
end

function Canvas:move_right()
  self.pos.x = self.pos.x + 1
  if self.pos.x > self.W then
    self.pos.x = 1
  end
end

function Canvas:move_up()
  self.pos.y = self.pos.y - 1
  if self.pos.y < 1 then
    self.pos.y = self.H
  end
end

function Canvas:move_down()
  self.pos.y = self.pos.y + 1
  if self.pos.y > self.H then
    self.pos.y = 1
  end
end

function Canvas:normal_mode()
--  print("Disegno in", self.pos.x, self.pos.y)
--  self.GRID[self.pos.x][self.pos.y] = self.palette:get_active_color_index()
  self.state = NORMAL
  self:put_pixel_now(self.pos.x, self.pos.y, self.palette:get_active_color_index())
end

function Canvas:sample()  
  self.palette:set_active_color(self.GRID[self.pos.x][self.pos.y])
end

function Canvas:fill()
  self:start_transaction()
  
  -- Recupero il colore sotto di me
  local colore_base = self.GRID[self.pos.x][self.pos.y]

  local colore_dest = self.palette:get_active_color_index()
  local punti = {}
  
  local visitati = {}
  for i = 1, self.W do
    visitati[i] = {}
    for j = 1, self.H do
      table.insert(visitati[i], false)
    end
  end
  
  table.insert(punti, {x = self.pos.x, y = self.pos.y})
  visitati[self.pos.x][self.pos.y] = true
  
  while #punti > 0 do
    local p = table.remove(punti)
    self:put_pixel(p.x, p.y, colore_dest)
    
    local x = p.x
    local y = p.y
    
    if x - 1 > 0 and self.GRID[x-1][y] == colore_base and visitati[x-1][y] == false then
      table.insert(punti, {x = x-1, y = y})
      visitati[x-1][y] = true
    end
    
    if x + 1 <= self.W and self.GRID[x+1][y] == colore_base and visitati[x+1][y] == false  then
      table.insert(punti, {x = x+1, y = y})
      visitati[x+1][y] = true
    end
    
    if y - 1 > 0 and self.GRID[x][y-1] == colore_base and visitati[x][y-1] == false  then
      table.insert(punti, {x = x, y = y-1})
      visitati[x][y-1] = true
    end
    
    if y + 1 <= self.H and self.GRID[x][y+1] == colore_base and visitati[x][y+1] == false  then
      table.insert(punti, {x = x, y = y+1})
      visitati[x][y+1] = true
    end
  end
  
  self:commit()
end

function Canvas:draw(x, y)
  -- Salvo i colori iniziali
  local ir,ig,ib,ia = lg.getColor()
  
  -- TODO
  -- Uso uno scaling fisso di 2x
  local CELL_WIDTH = self.CELL_WIDTH
  local BORDER = 5
  
  -- Disegno un quadrato di bordo
  lg.setColor(255, 255, 255)
  lg.rectangle("fill", x - BORDER, y - BORDER, CELL_WIDTH * self.W + BORDER * 2, CELL_WIDTH * self.H + BORDER * 2)
  
  -- Pulisco lo sprite batch
  self.sprite_batch:clear()

  for i = 1, self.W do
    for j = 1, self.H do
      local c = self.palette:get_color(self.GRID[i][j])

      self.sprite_batch:setColor(c.r, c.g, c.b)
      local pos_x = x + (i - 1) * CELL_WIDTH
      local pos_y = y + (j - 1) * CELL_WIDTH
--      lg.rectangle("fill", pos_x, pos_y, CELL_WIDTH, CELL_WIDTH)
      self.sprite_batch:add(pos_x, pos_y, 0, CELL_WIDTH)
    end
  end
  
  -- Disegno lo sprite_batch
  lg.draw(self.sprite_batch)
  
  -- Cursori
  local colore_cursore = self.palette:get_color(self.cursore.colore)  
  lg.setColor(colore_cursore.r, colore_cursore.g, colore_cursore.b)
  
  -- Inizio riga
  if self.state == LINE then
    -- Disegno una croce
    li = self.linea.inizio
    love.graphics.line(x + (li.x - 1) * CELL_WIDTH - 2, y + (li.y - 1) * CELL_WIDTH - 1, x + (li.x -1) * CELL_WIDTH + CELL_WIDTH + 2, y + (li.y - 1) * CELL_WIDTH + CELL_WIDTH + 2)
    love.graphics.line(x + (li.x - 1) * CELL_WIDTH + CELL_WIDTH + 2, y + (li.y - 1) * CELL_WIDTH - 1, x + (li.x -1) * CELL_WIDTH - 2, y + (li.y - 1) * CELL_WIDTH + CELL_WIDTH + 2)
    
  end
  
  -- Puntatore
  local x_punt = x + (self.pos.x - 1) * CELL_WIDTH - 1
  local y_punt = y + (self.pos.y - 1) * CELL_WIDTH - 1
  
  lg.rectangle("line", x_punt, y_punt, CELL_WIDTH + 2, CELL_WIDTH + 2)
  
  -- Modalità corrente
  lg.setColor(255, 255, 255)
  
  if self.state == NORMAL then
    lg.print("NORMAL", x, y + BORDER + CELL_WIDTH * self.H + BORDER)
  elseif self.state == PAINT then
    lg.print("PAINT", x, y + BORDER + CELL_WIDTH * self.H + BORDER)
  elseif self.state == LINE then
    lg.print("LINE", x, y + BORDER + CELL_WIDTH * self.H + BORDER)
  end
  
  -- Ripristino il colore iniziale
  lg.setColor(ir, ig, ib, ia)
end
